# Usage

Note: This is generic

## Arguments

* --square <double>

Squares a double. It can take as many doubles as you like. If it is not a number, it will fail.

* --root <double>

Gets the square root of a double. It can take as many doubles as you like. If it is not a number, it will fail.

* --add, -a <multiple doubles>

Adds all given doubles

* --multiply, -m <multiple doubles>

Multiplies the given doubles