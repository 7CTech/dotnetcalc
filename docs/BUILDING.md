# Building

## Windows:

First, install the [dotnet-core SDK](https://go.microsoft.com/fwlink/?LinkID=809122)

Next, clone the repo (`git clone https://gitlab.com/7CTech/dotnetcalc.git`)

`cd` into the cloned directory (Eg. `cd projects\dotnetcalc`)

Run `dotnet restore`

`cd src\dotnetcalc` to `cd` into the source directory

`dotnet build && dotnet publish -c Release` to build and publish the app

This results in a file called `dotnetcalc.dll`. To build an exe, copy everything in `project-standalone.json` into `project/json`.

Find your binary in `bin\Release\netcoreapp1.0\$platform\publish`, where $platform is your build platform (Eg. `win81-x84`, `win10-x64`, `win7-x64`, `win8`, etc.)



## GNU/Linux

First, install the dotnet-core SDK. Visit [Microsoft's Site](https://www.microsoft.com/net/core) to find the package right for your distribution.

Next, clone the repo (`git clone https://gitlab.com/7CTech/dotnetcalc.git`)

`cd` into the cloned directory (Eg. `cd projects/dotnetcalc`)

Run `dotnet restore`

`cd src/dotnetcalc` to `cd` into the source directory

`dotnet build && dotnet publish -c Release` to build and publish the app

This results in a file called `dotnetcalc.dll`. To build a binary, copy everything in `project-standalone.json` into `project.json`.

Find your binary in `bin/Release/netcoreapp1.0/$platform/publish`, where $platform is your build platform (Eg. `ubuntu.14.04-x64`, `ubuntu.16.10`, etc.)

Note, in my `runtime.json`, I only include Ubuntu 16.04 x64 and Ubuntu 14.04 x64. Look at the next point, and if your distribution is in that list, add it to `runtime.json` in the same format as the others. If you need help, file a bug.

Please note that only certain distributions included in [runtime.json](https://github.com/dotnet/corefx/blob/master/pkg/Microsoft.NETCore.Platforms/runtime.json) are supported. If your distribution isn't there, go file a bug.



## OS X

Okay, let's make something clear, I hate OS X, and I won't support it. It has stupid errors, and is a pain to build for. So if something doesn't work, don't blame me.

First, install the [dotnet-core SDK](https://www.microsoft.com/net/core#macos). There are some special instructions on that website on dependencies you need.

Next, clone the repo (`git clone https://gitlab.com/7CTech/dotnetcalc.git`)

`cd src/dotnetcalc` to `cd` into the source directory

`dotnet build && dotnet publish -c Release` to build and publish the app

This results in a file called `dotnetcalc.dll`. To build a binary, copy everything in `project-standalone.json` into `project.json`.

Find your binary in `bin/Release/netcoreapp1.0/$platform/publish`, where $platform is your build platform (Eg. `osx.10.12-x64`, `osx.10.11`, etc.)

