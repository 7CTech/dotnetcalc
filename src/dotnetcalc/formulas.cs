﻿/*
Copyright(c) 2016 7CTech

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;

namespace dotnetcalc
{
    public static class formulas
    {
        public static void formtest()
        {
            List<string> formulasList = new List<string>();
            addformula(formulasList, "volume");
            addformula(formulasList, "area");
            string[] formulasStrings = formulasList.ToArray();
            foreach (var formula in formulasStrings)
            {
                Console.WriteLine(formula);
            }
        }
        public static List<string> addformula(List<string> list, string itemtoadd)
        {
            list.Add(itemtoadd);
            return list;
        }
        public static string checkformula(List<string> formula, string userin) {
            for (int i = 0; i < formula.Count;) {
                if (formula[i] == userin) {
                    return userin;
                }
            }
            return "Error";
        }

        public static void addformuladev(formula userFormula, string name, string desc)
        {
            userFormula.name = name;
            userFormula.desc = desc;
        }
    }

    public class formula
    {
        public string name;

        public string desc;
    }
}
