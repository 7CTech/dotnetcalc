﻿/*
Copyright(c) 2016 7CTech

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using csargs = ncsargs.ccsargs;
using ncsargs;
using process = dotnetcalc.argprocess;

namespace dotnetcalc
{
    class Program
    {
        public static int Main(string[] args)
        {

            //Arguments - Dictionary of them all
            SortedDictionary<string, csarg> commands = new SortedDictionary<string, csarg>();
            //Initialise arguments
            //Main argument block
            csarg helparg = new csarg();
            csarg interactivearg = new csarg();
            csarg addarg = new csarg();
            csarg dividearg = new csarg();
            csarg expressionarg = new csarg();
            //csarg formulaarg = new csarg();
            csarg moduloarg = new csarg();
            csarg multiplyarg = new csarg();
            csarg rootarg = new csarg();
            csarg squarearg = new csarg();
            csarg subarg = new csarg();

            //Other arguments
            csarg quitarg = new csarg();

            //Assign them
            csargs.add(helparg, "help", "help", "show this message", "h");
            csargs.add(interactivearg, "interactive", "interactive", "interactive mode", "i");
            csargs.add(addarg, "add", "add", "add numbers together", "a");
            csargs.add(dividearg, "divide", "divide", "divide a number", "d");
            csargs.add(expressionarg, "expression", "expression", "evaluate an expression", "e");
            //csargs.add(formulaarg, "formula", "formula", "use a formula");
            csargs.add(moduloarg, "modulo", "modulo", "get the remainder of a division");
            csargs.add(multiplyarg, "multiply", "multiply", "multiply numbers");
            csargs.add(rootarg, "root", "root", "get the square root of a number");
            csargs.add(squarearg, "square", "square", "square a number");
            csargs.add(subarg, "subtract", "subtract", "subtract numbers from each other");

            //Other
            csargs.add(quitarg, "quit", "quit", "quit");

            //Put them in the dictionary
            commands.Add("1-help", helparg);
            commands.Add("add", addarg);
            commands.Add("divide", dividearg);
            commands.Add("expression", expressionarg);
            //commands.Add("formula", formulaarg);
            commands.Add("modulo", moduloarg);
            commands.Add("multiply", multiplyarg);
            commands.Add("root", rootarg);
            commands.Add("square", squarearg);
            commands.Add("subtract", subarg);

            //Interactive
            SortedDictionary<string, csarg> commandsInteractive = new SortedDictionary<string, csarg>(commands);
            commandsInteractive.Add("zz_quit", quitarg);
            commands.Add("2-interactive", interactivearg);

            /*Formulas
            formula helpFormula = new formula();
            formula areaFormula = new formula();
            formulas.addformuladev(helpFormula, "help", "show this message");
            formulas.addformuladev(areaFormula, "Area", "calculate");
            List<string> formulaList = new List<string>();
            formulas.addformula(formulaList, "help");
            formulas.addformula(formulaList, "area");
            */
            for (int a = 0; a < args.Length; a++)
            {
                if (args.Length < 2)
                {
                    if (csargs.check(helparg, args[a]))
                    {
                        csargs.show(commands);
                        return 0;
                    }
                    if (csargs.check(interactivearg, args[a]))
                    {
                        int interactiveResult = interactive(commandsInteractive);
                        if (interactiveResult == 0)
                        {
                            return 0;
                        }
                        return 1;
                    }
                    csargs.show(commands);
                    return 1;
                }
                if (!(args[a].StartsWith("-"))) //Parameters
                {
                    break;
                }
                else
                {
                    if (csargs.check(addarg, args[a]))
                    {
                        string result = "0";
                        for (int i = 1; i < args.Length;)
                        {
                            result = process.process(args[i], addarg, result);
                            if (result == "NaN") {
                                Console.WriteLine("Parameter at position " + i + " is not a number...");
                                Console.WriteLine("Exiting...");
                                return 1;
                            }
                            if (i == args.Length - 1)
                            {
                                Console.WriteLine(result);
                                return 0;
                            }
                            i++;
                        }
                    }
                    if (csargs.check(dividearg, args[a]))
                    {
                        if (args.Length < 2)
                        {
                            Console.WriteLine("More detail required");
                            return 1;
                        }
                        string result = "0";
                        for (int i = 2; i < args.Length;)
                        {
                            if (i == 2)
                            {
                                result = process.process(args[i], dividearg, args[i - 1]);
                            }
                            else
                            {
                                result = process.process(args[i], dividearg, result);
                            }
                            if (result == "NaN")
                            {
                                Console.WriteLine("Parameter at position " + i + " is not a number...");
                                Console.WriteLine("Exiting...");
                                return 1;
                            }
                            if (i == args.Length - 1)
                            {
                                Console.WriteLine(result);
                                return 0;
                            }
                            i++;
                        }
                    }
                    if (csargs.check(expressionarg, args[a]))
                    {
                        if (args.Length < 2)
                        {
                            Console.WriteLine("More detail required for an expression");
                            return 1;
                        }
                        string useroutString = dotnetcalc.expressions.parseExpression(args);
                        if (useroutString == "Error")
                        {
                            return 1;
                        }
                        Console.WriteLine(useroutString);
                        return 0;
                    }
                    /*if (csargs.check(formulaarg, args[a])) {
                        for (int i = 1; i < args.Length;) {
                            string userin = formulas.checkformula(formulaList, args[i]);
                            if (userin == "Error") {
                                Console.WriteLine("An error has occurred");
                                return 1;
                            }
                            if (userin == "help")
                            {
                                for (int e = 0; e < formulaList.Count();) {
                                    Console.WriteLine(formulaList[e]);
                                    e++;
                                    if (e == formulaList.Count)
                                    {
                                        return 0;
                                    }
                                }
                            }
                            if (userin == "area") {
                                Console.WriteLine("Area");
                            }
                            if (i == args.Length - 1) {
                                return 0;
                            }
                            i++;
                        }
                    }*/
                    if (csargs.check(multiplyarg, args[a]))
                    {
                        string result = "1";
                        for (int i = 1; i < args.Length;)
                        {
                            result = process.process(args[i], multiplyarg, result);
                            if (result == "NaN")
                            {
                                Console.WriteLine("Parameter at position " + i + " is not a number...");
                                Console.WriteLine("Exiting...");
                                return 1;
                            }
                            if (i == args.Length - 1)
                            {
                                Console.WriteLine(result);
                                return 0;
                            }
                            i++;
                        }
                    }
                    if (csargs.check(moduloarg, args[a]))
                    {
                        if (args.Length < 3)
                        {
                            Console.WriteLine("More detail required");
                            csargs.show(commands);
                            return 1;
                        }
                        string result = "0";
                        for (int i = 2; i < args.Length;)
                        {
                            if (i == 2)
                            {
                                result = process.process(args[i], moduloarg, args[i - 1]);
                            }
                            else
                            {
                                result = process.process(args[i], moduloarg, result);
                            }

                            if (result == "NaN")
                            {
                                Console.WriteLine("Parameter at position " + i + " is not a number...");
                                Console.WriteLine("Exiting...");
                                return 1;
                            }
                            if (i == args.Length - 1)
                            {
                                Console.WriteLine(result);
                                return 0;
                            }
                            i++;

                        }
                    }
                    if (csargs.check(rootarg, args[a]))
                    {
                        for (int i = 1; i < args.Length;)
                        {
                            var result = process.process(args[i], rootarg);
                            if (result == "NaN")
                            {
                                Console.WriteLine("Parameter at position " + i + " is not a number...");
                                Console.WriteLine("Exiting...");
                                return 1;
                            }
                            Console.WriteLine(result);
                            if (i == args.Length - 1)
                            {
                                return 0;
                            }
                            i++;
                        }
                    }
                    if (csargs.check(squarearg, args[a]))
                    {
                        for (int i = 1; i < args.Length;)
                        {
                            var result = process.process(args[i], squarearg);
                            if (result == "NaN")
                            {
                                Console.WriteLine("Parameter at position " + i + " is not a number...");
                                Console.WriteLine("Exiting...");
                                return 1;
                            }
                            Console.WriteLine(result);
                            if (i == args.Length - 1)
                            {
                                return 0;
                            }
                            i++;
                        }
                    }
                    if (csargs.check(subarg, args[a]))
                    {
                        if (args.Length < 3)
                        {
                            Console.WriteLine("More detail required");
                            csargs.show(commands);
                            return 1;
                        }
                        string result = "0";
                        for (int i = 2; i < args.Length;)
                        {
                            if (i == 2)
                            {
                                result = process.process(args[i], subarg, args[i-1]);
                            }
                            else
                            {
                                result = process.process(args[i], subarg, result);
                            }
                            if (result == "NaN")
                            {
                                Console.WriteLine("Parameter at position " + i + " is not a number...");
                                Console.WriteLine("Exiting...");
                                return 1;
                            }
                            if (i == args.Length - 1)
                            {
                                Console.WriteLine(result);
                                return 0;
                            }
                            i++;
                        }
                    }
                }
            }
            csargs.show(commands);
            return 1;
        }

        public static int interactive(SortedDictionary<string, csarg> commands)
        {
            start_interactive:
            Console.WriteLine();
            Console.WriteLine("Please enter your operation. 'help' for help: ");
            string operation = Console.ReadLine();
            Console.WriteLine();
            if (operation == "help")
            {
                for (int i = 0; i < commands.Count; i++)
                {
                    Console.WriteLine(commands.ElementAt(i).Value.name +
                                      ": ".PadRight(30 - commands.ElementAt(i).Value.name.Length) +
                                      commands.ElementAt(i).Value.desc);
                }
                goto start_interactive;
            }
            if (operation == "quit")
            {
                return 0;
            }
            string result = "0";
            if (operation == "add")
            {
                csarg addarg = commands["add"];
                Console.WriteLine("Enter the numbers to add: ");
                string userString = Console.ReadLine();
                Console.WriteLine();
                string[] numbersGiven = userString.Split();
                for (int i = 0; i < numbersGiven.Length;)
                {
                    result = process.process(numbersGiven[i], addarg, result);
                    if (result == "NaN")
                    {
                        Console.WriteLine("Parameter at position " + (i + 1) + " is not a number");
                        Console.WriteLine("Exiting...");
                        return 1;
                    }
                    if (i == numbersGiven.Length - 1)
                    {
                        Console.WriteLine(result);
                        return 0;
                    }
                    i++;
                }
            }
            if (operation == "divide")
            {
                csarg dividearg = commands["divide"];
                Console.WriteLine("Enter the numbers to divide: ");
                string userString = Console.ReadLine();
                Console.WriteLine();
                string[] numbersGiven = userString.Split();
                for (int i = 0; i < numbersGiven.Length;)
                {
                    if (numbersGiven.Length <= 1)
                    {
                        Console.WriteLine("More detail required");
                        return 1;
                    }
                    if (i == 1)
                    {
                        result = process.process(numbersGiven[i], dividearg, numbersGiven[i - 1]);
                    }
                    else
                    {
                        result = process.process(numbersGiven[i], dividearg, result);
                    }
                    if (result == "NaN")
                    {
                        Console.WriteLine("Parameter at position " + (i + 1) + " is not a number");
                        Console.WriteLine("Exiting...");
                        return 1;
                    }
                    if (i == numbersGiven.Length - 1)
                    {
                        Console.WriteLine(result);
                        return 0;
                    }
                    i++;
                }
            }
            if (operation == "expression")
            {
                Console.WriteLine("Enter your expression: ");
                string userInString = Console.ReadLine();
                string userOutString = expressions.parseInteractiveExpression(userInString);
                if (userOutString == "Error")
                {
                    return 1;
                }
                Console.WriteLine(userOutString);
                return 0;
            }
            if (operation == "multiply")
            {
                result = "1";
                csarg multiplyarg = commands["multiply"];
                Console.WriteLine("Enter the numbers to multiply: ");
                string userString = Console.ReadLine();
                Console.WriteLine();
                string[] numbersGiven = userString.Split();
                for (int i = 0; i < numbersGiven.Length;)
                {
                    result = process.process(numbersGiven[i], multiplyarg, result);
                    if (result == "NaN")
                    {
                        Console.WriteLine("Parameter at position " + (i + 1) + " is not a number");
                        Console.WriteLine("Exiting...");
                        return 1;
                    }
                    if (i == numbersGiven.Length - 1)
                    {
                        Console.WriteLine(result);
                        return 0;
                    }
                    i++;
                }
            }
            if (operation == "modulo")
            {
                csarg moduloarg = commands["modulo"];
                Console.WriteLine("Enter the numbers you would like to the get remainder of after a division: ");
                string userString = Console.ReadLine();
                Console.WriteLine();
                string[] numbersGiven = userString.Split();
                for (int i = 0; i < numbersGiven.Length;)
                {
                    if (numbersGiven.Length <= 1)
                    {
                        Console.WriteLine("More detail required");
                        return 1;
                    }
                    if (i == 1)
                    {
                        result = process.process(numbersGiven[i], moduloarg, numbersGiven[i - 1]);
                    }
                    else
                    {
                        result = process.process(numbersGiven[i], moduloarg, result);
                    }
                    if (result == "NaN")
                    {
                        Console.WriteLine("Parameter at position " + (i + 1) + " is not a number");
                        Console.WriteLine("Exiting...");
                        return 1;
                    }
                    if (i == numbersGiven.Length - 1)
                    {
                        Console.WriteLine(result);
                        return 0;
                    }
                    i++;
                }
            }
            if (operation == "root")
            {
                csarg rootarg = commands["root"];
                Console.WriteLine("Enter the numbers you would like to the get square root of: ");
                string userString = Console.ReadLine();
                Console.WriteLine();
                string[] numbersGiven = userString.Split();
                for (int i = 0; i < numbersGiven.Length;)
                {
                    result = process.process(numbersGiven[i], rootarg);
                    if (result == "NaN")
                    {
                        Console.WriteLine("Parameter at position " + (i + 1) + " is not a number");
                        return 1;
                    }
                    Console.WriteLine(result);
                    if (i == numbersGiven.Length - 1)
                    {
                        return 0;
                    }
                    i++;
                }
            }
            if (operation == "square")
            {
                csarg squarearg = commands["square"];
                Console.WriteLine("Enter the numbers you would like to the get square of: ");
                string userString = Console.ReadLine();
                Console.WriteLine();
                string[] numbersGiven = userString.Split();
                for (int i = 0; i < numbersGiven.Length;)
                {
                    result = process.process(numbersGiven[i], squarearg);
                    if (result == "NaN")
                    {
                        Console.WriteLine("Parameter at position " + (i + 1) + " is not a number");
                        Console.WriteLine("Exiting...");
                        return 1;
                    }
                    Console.WriteLine(result);
                    if (i == numbersGiven.Length - 1)
                    {
                        return 0;
                    }
                    i++;
                }
            }
            if (operation == "subtract")
            {
                csarg subarg = commands["subtract"];
                Console.WriteLine("Enter the numbers you would like to subtract: ");
                string userString = Console.ReadLine();
                Console.WriteLine();
                string[] numbersGiven = userString.Split();
                if (numbersGiven.Length < 2)
                {
                    Console.WriteLine("More detail required");
                    return 1;
                }
                for (int i = 0; i < numbersGiven.Length;)
                {
                    if (i == 1)
                    {
                        result = process.process(numbersGiven[i], subarg, numbersGiven[i - 1]);
                    }
                    else
                    {
                        result = process.process(numbersGiven[i], subarg, result);
                    }
                    if (result == "NaN")
                    {
                        Console.WriteLine("Parameter at position " + (i + 1) + " is not a number");
                        Console.WriteLine("Exiting...");
                        return 1;
                    }
                    if (i == numbersGiven.Length - 1)
                    {
                        Console.WriteLine(result);
                        return 0;
                    }
                    i++;
                }
            }
            else
            {
                Console.WriteLine("Unknown operation specified");
                goto start_interactive;
            }
            return 1;
        }
    }
}
