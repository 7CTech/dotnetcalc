﻿/*
Copyright(c) 2016 7CTech

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System;
using ncsargs;
using dncop = dotnetcalc.operations;

namespace dotnetcalc
{
    public static class argprocess
    {
        public static string process(string arg, csarg userarg, string snumbase = "0")
        {

            double result = 0;
            double userin;
            if (Double.TryParse(arg, out userin)) //try to make the string input into a double
            {
                if (userarg.name == "add")
                {
                    double dnumbase = Double.Parse(snumbase);
                    result = dncop.add(userin, dnumbase);
                }
                if (userarg.name == "multiply")
                {
                    double dnumbase = Double.Parse(snumbase);
                    result = dncop.multiply(userin, dnumbase);
                }
                if (userarg.name == "modulo")
                {
                    double dnumbase = Double.Parse(snumbase);
                    result = dncop.modulo(userin, dnumbase);
                }
                if (userarg.name == "divide")
                {
                    double dnumbase = Double.Parse(snumbase);
                    result = dncop.divide(userin, dnumbase);
                }
                if (userarg.name == "root")
                {
                    result = dncop.root(userin);
                }
                if (userarg.name == "square")
                {
                    result = dncop.square(userin);
                }
                if (userarg.name == "subtract")
                {
                    double dnumbase = Double.Parse(snumbase);
                    result = dncop.subtract(userin, dnumbase);
                }
                string stringres = result.ToString();
                return stringres;
            }
            else
            {
                return "NaN";
            }
        }
    }
}