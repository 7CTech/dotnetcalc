﻿using System;
using NCalc;

namespace dotnetcalc
{
    public static class expressions
    {
        public static string parseExpression(string[] userArgs)
        {
            string userInString = "beligh";
            for (int i = 1; i < userArgs.Length; i++)
            {
                if (i == 1) {
                    userInString = userArgs[i] + " ";
                }
                else {
                    if (i == userArgs.Length - 1) {
                        userInString = userInString + userArgs[i];
                    }
                    else {
                        userInString = userInString + userArgs[i] + " ";
                    }
                }
            }
            var expression = new Expression(userInString);
            if (expression.HasErrors())
            {
                Console.WriteLine("Error with expression");
                return "Error";
            }
            var userOutDouble = expression.Evaluate();
            string userOutString = userOutDouble.ToString();
            return userOutString;
        }

        public static string parseInteractiveExpression(string userInString)
        {
            var expression = new Expression(userInString);
            if (expression.HasErrors())
            {
                Console.WriteLine("Error with expression");
                return "Error";
            }
            var useroutDouble = expression.Evaluate();
            string useroutString = useroutDouble.ToString();
            return useroutString;
        }
    }
}
