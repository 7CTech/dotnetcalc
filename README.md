# dotnetcalc

[![Build status](https://ci.appveyor.com/api/projects/status/9d6bf6d446ug3eh9?svg=true)](https://ci.appveyor.com/project/7CTech/dotnetcalc)

A simple C#/dotnet command-line calculator



View the [BUILDING INSTRUCTIONS](https://gitlab.com/7CTech/dotnetcalc/blob/master/docs/BUILDING.md) to build

View the [USAGE INSTRUCTIONS](https://gitlab.com/7CTech/dotnetcalc/blob/master/docs/USAGE.md) for usage instructions