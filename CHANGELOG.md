# VERSIONING:

Everything 0.0.* is alpha. Consider is HIGHLY unstable.

Everything 0.* is beta. Digits after that indicate a minor update.

Everything 1+.* is stable. Where the last 2 digits are MAJOR Patch, Then Minor Patch.

## Examples:

### Alpha
1st Alpha: 0.0.1

2nd Alpha: 0.0.2

### Beta
1st Beta: 0.1.0

1st Beta with a minor update: 0.1.1

Second Beta: 0.2.0

Second Beta with 3rd minor patch: 0.2.3

### Stable

First Stable: 1.0.0

First Stable with 1 minor patch: 1.0.1

First Stable with 1 major patch: 1.1.0

First Stable with 1 major and 1 minor patch : 1.1.1







## Version 0.0.1
* Function square
* Function root
* No error handling


## Version 0.0.2
* csargs
* square
* root
* add
* csargs was rewritten

## Version 0.0.3
* csargs
* square
* root
* add
* multiply
* added an [argument processor](https://gitlab.com/7CTech/dotnetcalc/blob/master/src/dotnetcalc/argprocess.cs)

## Version 0.0.4
* csargs
* interactive-mode
* add
* divide
* modulo
* multiply
* root
* square
* subtract
* added divide, modulo, subtract
* added interactive-mode

## Version 0.0.5
* csargs
* interactive-mode
* add
* divide
* expressions
* modulo
* multiply
* root
* square
* subtract
* fixed a crashing bug
* added expressions